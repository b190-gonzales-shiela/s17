console.log("Hello!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function userName(){

		let fullName=prompt("Enter your fullname");
		let userAge=prompt("Enter your age");
		let userLocation=prompt("Enter your location");

		console.log("Hello"+ " " + fullName);
		console.log("You are" + userAge + " " + "years old");
		console.log("You live in "+" " + userLocation);
		
	};

	userName();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

	
*/

	//second function here:

	function faveBand(){

		let faveBand1= "1. The Beatles";
		let faveBand2= "2. The Corrs";
		let faveBand3= "3. The Cranberries";
		let faveBand4= "4. Red hot Chilli Peppers";
		let faveBand5= "5. Oasis"

		console.log(faveBand1);
		console.log(faveBand2);
		console.log(faveBand3);
		console.log(faveBand4);
		console.log(faveBand5);

	
	};

	faveBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
		function faveMovie(){

		let faveMovie1= "1. Tokyo Drift: Fast & the Furious";
		let rottenTomato1=" Rotten tomatoes Rating: 38% "

		let faveMovie2= "2. Harry Potter Series";
		let rottenTomato2=" Rotten tomatoes Rating: 81% "

		let faveMovie3= "3. Miss Peregrines home of peculiar children";
		let rottenTomato3=" Rotten tomatoes Rating: 64% "

		let faveMovie4= "4. Lord of the Rings";
		let rottenTomato4=" Rotten tomatoes Rating: 91% "

		let faveMovie5= "5. Heneral Luna"
		let rottenTomato5=" Rotten tomatoes Rating: 75% "

		console.log(faveMovie1);
		console.log(rottenTomato1);
		console.log(faveMovie2);
		console.log(rottenTomato2);
		console.log(faveMovie3);
		console.log(rottenTomato3);
		console.log(faveMovie4);
		console.log(rottenTomato4);
		console.log(faveMovie5);
		console.log(rottenTomato5);

	
	};

	faveMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	// printUsers();
	function printFriends(){

	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log("You are friends with:"+ " " + friend1); 
	console.log("You are friends with:"+ " " + friend2); 
	console.log("You are friends with:"+ " " + friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);